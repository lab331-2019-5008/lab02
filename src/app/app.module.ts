import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImpl2Service } from './service/student-data-impl2.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: StudentService, useClass: StudentDataImpl2Service}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
