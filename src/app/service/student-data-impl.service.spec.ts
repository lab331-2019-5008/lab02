import { TestBed } from '@angular/core/testing';

import { StudentDataImplService } from './student-data-impl.service';

describe('StudentDataImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentDataImplService = TestBed.get(StudentDataImplService);
    expect(service).toBeTruthy();
  });
});
