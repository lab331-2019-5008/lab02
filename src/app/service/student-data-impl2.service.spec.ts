import { TestBed } from '@angular/core/testing';

import { StudentDataImpl2Service } from './student-data-impl2.service';

describe('StudentDataImpl2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentDataImpl2Service = TestBed.get(StudentDataImpl2Service);
    expect(service).toBeTruthy();
  });
});
