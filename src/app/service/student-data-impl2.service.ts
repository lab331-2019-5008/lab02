import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Student } from '../entity/student';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentDataImpl2Service extends StudentService{
  getStudent(): Observable<Student[]>{
    return of(this.students);
  }

  students: Student[] = [{
    id: 1,
    studentId: '5621105017',
    name: 'Eiei',
    surname: 'Za55+',
    gpa: 4.00
  }, {
    id: 2,
    studentId: '5621105029',
    name: 'Ae',
    surname: 'Parina',
    gpa: 2.12
  }]
  constructor() { 
    super()
  }
}
