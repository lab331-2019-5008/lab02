import { Observable } from "rxjs";
import { Student } from "../entity/student";

export abstract class StudentService {
    abstract getStudent(): Observable<Student[]>;
}
